import "./App.css";
import Header from "./Components/Header";
import Todos from "./Components/Todos";
import List from "./Components/List";
import ViewOption from "./Components/ViewOption";
import React from "react";

function App() {
	const getTodosLocalStorage = () => {
		return JSON.parse(localStorage.getItem("todos"));
	};
	const [todos, setTodos] = React.useState(getTodosLocalStorage() || []);

	const setTodosLocalStorage = (todos) => {
		localStorage.setItem("todos", JSON.stringify(todos));
	};
	const addTodo = (todo) => {
		const newTodos = [todo, ...todos];

		setTodos(newTodos);
		setTodosLocalStorage(newTodos);
	};
	const toggleTodoStatus = (todo) => {
		const updatedTodos = todos.map((item) => {
			if (item === todo) {
				item.status = !item.status;
			}
			return item;
		});
		setTodos(updatedTodos);
		setTodosLocalStorage(updatedTodos);
	};

	const removeTodo = (todo) => {
		const updatedTodos = todos.filter((item) => item !== todo);
		setTodos(updatedTodos);
		setTodosLocalStorage(updatedTodos);
	};

	const filterCompleted = () => {
		const savedTodos = getTodosLocalStorage();
		const filteredTodos = savedTodos.filter((item) => item.status === true);
		setTodos(filteredTodos);
	};

	const filterActive = () => {
		const savedTodos = getTodosLocalStorage();
		const filteredTodos = savedTodos.filter((item) => item.status === false);
		setTodos(filteredTodos);
	};

	const filterAll = () => {
		const savedTodos = getTodosLocalStorage();
		setTodos(savedTodos);
	};

	const filter = (val) => {
		if (val === "completed") filterCompleted();
		else if (val === "active") filterActive();
		else filterAll();
	};

	const sortStatus = () => {
		const sortedTodos = [...todos];
		sortedTodos.sort((x, y) => {
			return x.status === y.status ? 0 : x.status ? 1 : -1;
		});
		setTodos(sortedTodos);
	};

	const sortAlphabetically = () => {
		const sortedTodos = [...todos];
		sortedTodos.sort((a, b) => a.text.localeCompare(b.text));
		setTodos(sortedTodos);
	};

	const sort = (val) => {
		if (val === "status") sortStatus();
		else sortAlphabetically();
	};

	return (
		<div className="App" class="container mt-2">
			<Header />
			<Todos onSubmit={addTodo} />
			<ViewOption filter={filter} sort={sort}></ViewOption>
			{todos.map((todo, index) => (
				<List
				todos={todos}
				setTodos={setTodos}
				setTodosLocalStorage={setTodosLocalStorage}
				toggleTodoStatus={toggleTodoStatus}
				removeTodo={removeTodo}
				todo={todo}
				key={index}	  
				></List>
			))}
		</div>
	);
}

export default App;

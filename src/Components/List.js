import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen, faTrash } from "@fortawesome/free-solid-svg-icons";
import React from "react";
import { faSquare, faCheckSquare } from "@fortawesome/free-regular-svg-icons";
import ModalTodo from "./Modal";
function List({
	todo,
	todos,
	setTodos,
	setTodosLocalStorage,
	toggleTodoStatus,
	removeTodo,
}) {
	const [modalShow, setModalShow] = React.useState(false);
	const showEdit = () => {
		setModalShow(true);
	};

	const updateTodoText = (text) => {
		const updatedTodos = todos.map((item) => {
			if (item === todo) {
				item.text = text;
			}
			return item;
		});
		setTodos(updatedTodos);
		setTodosLocalStorage(updatedTodos);
	};

	const onHide = () => {
		setModalShow(false);
	};

	return (
		<div
			class="container rounded-lg mt-2 border-left"
			style={{ backgroundColor: "#393e52" }}
		>
			<div class="row mx-1 px-5 pb-3 w-80">
				<div class="col mx-auto">
					<div class="row px-3 align-items-center todo-item rounded">
						<div class="col-auto m-1 p-0 d-flex align-items-center">
							<h2 class="m-0 p-0">
								<FontAwesomeIcon
									icon={faSquare}
									style={{ fontSize: "2rem" }}
									onClick={() => toggleTodoStatus(todo)}
									className={
										"text-primary m-0 p-0 btn" + (todo.status ? " d-none" : "")
									}
									title="Mark as complete"
								></FontAwesomeIcon>
								<FontAwesomeIcon
									icon={faCheckSquare}
									style={{ fontSize: "2rem" }}
									onClick={() => toggleTodoStatus(todo)}
									className={
										"text-primary m-0 p-0 btn" + (todo.status ? "" : " d-none")
									}
									title="Mark as todo"
								></FontAwesomeIcon>
							</h2>
						</div>
						<div class="col px-1 m-1 d-flex align-items-center">
							<div class="form-control form-control-lg border-0 bg-transparent rounded px-3 text-secondary font-weight-bold h-auto text-center">
								<span>{todo.text}</span>
							</div>
						</div>
						<div class="col-auto m-1 p-0 px-3 d-none"></div>
						<div class="col-auto m-1 p-0 todo-actions">
							<div class="row d-flex align-items-center justify-content-end">
								<h5 class="m-0 p-0 px-2">
									<FontAwesomeIcon
										icon={faPen}
										className="text-info btn m-0 p-0"
										onClick={() => showEdit()}
									></FontAwesomeIcon>
									<ModalTodo
										todo={todo}
										show={modalShow}
										// updateTodoText={updateText}
										updateTodoText={updateTodoText}
										onHide={onHide}
									></ModalTodo>
								</h5>
								<h5 class="m-0 p-0 px-2">
									<FontAwesomeIcon
										icon={faTrash}
										className="text-danger btn m-0 p-0"
										onClick={() => removeTodo(todo)}
									></FontAwesomeIcon>
								</h5>
							</div>
							<div class="row todo-created-info">
								<div class="col-auto d-flex align-items-center pr-2"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default List;

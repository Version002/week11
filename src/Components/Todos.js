import { useState } from "react";

function Todos(props) {
	const [input, setInput] = useState("");

	const handleChange = (e) => {
		setInput(e.target.value);
	};

	const handleSubmit = (e) => {
		e.preventDefault();

		props.onSubmit({
			text: input,
			status: false,
		});

		setInput("");
	};

	return (
		<form
			className="d-flex mb-4 justify-content-center flex-column align-items-center"
			onSubmit={handleSubmit}
		>
			<div className="container col-8">
				<input
					className="form-control form-control-lg border-0 input-box rounded text-secondary"
					type="text"
					placeholder="Add new todo.."
					value={input}
					onChange={handleChange}
				/>
			</div>
			<div className="col-auto px-0 mx-0 mr-2 mt-2">
				<button className="btn btn-primary">Add Todo</button>
			</div>
		</form>
	);
}

export default Todos;
// const List = () => {
//     return (
//         <div>
//             <div class="d-flex justify-content-between align-items-center">
//                 <h3 class="text-light">Todo List:</h3>
//                 <div class="dropdown">
//                     <button class="btn btn-outline-secondary dropdown-toggle text-light" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
//                         Sort By
//                     </button>
//                     <div class="dropdown-menu dropBox" aria-labelledby="dropdownMenuButton">
//                         <div class="my-2 p-0 d-flex align-items-center justify-content-between dropdown-item hovered" id="sort-alpha">
//                             <span class="dropdown-item p-0 ml-3 text-muted hovered">Alphabetically</span>
//                             <i class="fas fa-check mr-3 text-muted hovered" id="check-alpha"></i>
//                         </div>
//                         <div class="my-2 p-0 d-flex align-items-center justify-content-between dropdown-item hovered" id="sort-com">
//                             <span class="p-0 ml-3 text-muted hovered">Completed</span>
//                             <i class="fas fa-check mr-3 text-muted hovered" id="check-com"></i>
//                         </div>
//                     </div>
//                 </div>
//             </div>
//             <div class="rounded mt-5" id="todoList">
//                 {/* todolist goes here */}
//             </div>
//         </div>
//     )
// }

// export default List;


const Header = () => {
    return (
        <div className="row m-1 p-4">
      <div className="col">
        <div className="p-1 h1 text-primary text-center mx-auto display-inline-block">
          <h3>TASK 1: ToDo List with React</h3>
        </div>
      </div>
    </div>
    )
}

export default Header;